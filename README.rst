=====
Autos Common
=====

Autos Common is a stand alone Django app to sync models between the CDA and the DMS.

Detailed documentation can be found here: 
 https://confluence.usnews.com/pages/viewpage.action?spaceKey=AUTOS&title=Autos+Model+Django+Package

Quick start
-----------

1. Add "autos_common" to your INSTALLED_APPS setting like this::

    INSTALLED_APPS = [
        ...
        'autos_common',
    ]

2. Run ``python manage.py migrate`` to create the autos_common models.
