from django.apps import AppConfig

class AutosModelsAppConfig(AppConfig):
	name = 'autos_common'
	verbose_name = 'Autos Common'

	def ready(self):
		# code to be executed once app is ready goes here
		# eg: signal handlers
		pass