#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.utils import timezone

from ..model_mixins import ScheduledPublishMixin, DealsAppMixin

OFFER_TYPE_CHOICES = (
    ('Cash Back', 'Cash Back'),
    ('Lease', 'Lease'),
    ('Finance', 'Finance')
)

STATUS_CHOICES = (
    ('ARCHIVED', 'Archived'),
    ('DRAFT', 'Draft'),
    ('APPROVED', 'Approved'),
    ('PUBLISHED', 'Published'),
    ('FUTURE_QA', 'Scheduled Publish to QA'),
    ('FUTURE_PROD', 'Scheduled Publish to prod'),
)


class CarDealQuerySet(models.QuerySet):
    # TODO: have these work outside the autos-common app (use the reversion table)

    # def in_revision_of_version(self, version_id):
    #     """
    #     Given an ID of a Version instance (which is in the URL of the history page), gets
    #     all of the deals which have a Version associated with the Revision.
    #     :param version_id: a Version's id
    #     :type version_id: int
    #     :return: a filtered QuerySet
    #     :rtype: CarDealQuerySet
    #     """
    #
    #     # We have to get the revision ID from the Version, which gets us all of the other related versions, which have the object IDs
    #     # which can in turn be used to filter the list of child car deals we're serving with this page.
    #     return self.filter(
    #         id__in=Version.objects.filter(revision_id=Version.objects.get(pk=version_id).revision_id).values_list('object_id')
    #     )

    # def not_in_revision_of_version(self, version_id):
    #
    #     """
    #     Given an ID of a Version instance (which is in the URL of the history page), gets
    #     all of the deals which DON'T have a Version associated with the Revision.
    #     :param version_id: a Version's id
    #     :type version_id: int
    #     :return: a filtered QuerySet
    #     :rtype: CarDealQuerySet
    #     """
    #
    #     # We have to get the revision ID from the Version, which gets us all of the other related versions, which have the object IDs
    #     # which can in turn be used to filter the list of child car deals we're serving with this page.
    #     return self.exclude(
    #         id__in=Version.objects.filter(revision_id=Version.objects.get(pk=version_id).revision_id).values_list('object_id')
    #     )

    def archive(self, username):
        return self.update(status='ARCHIVED',
                           is_active=0,
                           lastupdate=timezone.now(),
                           lastupdateuser=username)

    def draft(self):
        return self.filter(status='DRAFT')

    def published(self):
        return self.filter(status='PUBLISHED')

    def approved(self):
        return self.filter(status='APPROVED')

    def archived(self):
        return self.filter(status='ARCHIVED')

    def not_archived(self):
        return self.exclude(status='ARCHIVED')

    def feed_deals(self):
        return self.filter(is_feed_deal=1)

    def custom_deals(self):
        return self.filter(is_feed_deal=0)


class CarDealer(DealsAppMixin, models.Model):
    dealer_makes = models.CharField(max_length=200)
    dealer_internal_id = models.CharField(max_length=30)
    name = models.CharField(max_length=200, blank=True, null=True)
    address = models.CharField(max_length=250, blank=True, null=True)
    city = models.CharField(max_length=100, blank=True, null=True)
    state_code = models.CharField(max_length=2, blank=True, null=True)
    zip_code = models.CharField(max_length=10)
    region = models.CharField(max_length=250, blank=True, null=True)
    district = models.CharField(max_length=250, blank=True, null=True)
    date_created = models.DateTimeField(blank=True, null=True)
    date_modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        db_table = "deals_v2_cardealer"


class CarDeal(DealsAppMixin, ScheduledPublishMixin):
    """
    Table in charge of car deals content

    The deals here are generated from an rr-script.
    - Each deal has 1 product
    - EVA architechture was used here so if you're looking for additional fields we
    have seperate table for attributes and types
    - "Approve" is part of the publishing proccess and so is "future publish".

    5) DRAFTS = https://code.usnews.com/its/realestate/blob/master/bestplaces/models.py#L66

    If `ScheduledPublishMixin` is removed models.Model needs to be imported

    For the `description` field to have richtext do this in the admin:

    from ckeditor.widgets import CKEditorWidget

    from foo.models import Bar

    class BarAdminForm(forms.ModelForm):
        description = forms.CharField(widget=CKEditorWidget())
    """
    name = models.CharField(max_length=100)
    offer_type = models.CharField(choices=OFFER_TYPE_CHOICES, max_length=100, blank=True,
                                  null=True)
    description = models.TextField(blank=True, default='<p></p>')
    product_id = models.IntegerField()
    end_date = models.DateTimeField(blank=True, null=True, verbose_name='Expires')
    start_date = models.DateTimeField(blank=True, null=True, verbose_name='Starting')
    date_modified = models.DateTimeField(verbose_name='Date Last Updated', auto_now=True)
    last_update_user = models.CharField(max_length=100, blank=True, null=True,
                                      verbose_name='Last Updated By')
    date_created = models.DateTimeField(verbose_name='Date Created', auto_now_add=True)
    dealer = models.ForeignKey('CarDealer', on_delete=models.CASCADE)
    status = models.CharField(choices=STATUS_CHOICES, max_length=100, blank=True, null=True)

    objects = CarDealQuerySet.as_manager()

    class Meta:
        db_table = "deals_v2_cardeal"

    def __unicode__(self):
        return self.name.strip()

    def __str__(self):
        return self.name.strip()

    @property
    def summary(self):
        return self.description

    # TODO: These need to be implemented in order for scheduled publishing to work
    # TODO: at the same time we need to see if CDA needs the django_rq package on the CDA
    # def scheduled_publish(self):
    #     if self.status == 'FUTURE_QA':
    #         self.publish(is_preview=True)
    #         return "Successfully published {} to QA".format(str(self))
    #     elif self.status == 'FUTURE_PROD':
    #         self.publish(is_preview=False)
    #         return "Successfully published {} to production".format(str(self))
    #     else:
    #         raise ValueError('Status is {} when we are trying to do a scheduled publish'.format(self.status))
    #
    # def ready_for_scheduled_publish(self):
    #     return self.status in ('FUTURE_QA', 'FUTURE_PROD', )


class CarDealAttributeType(DealsAppMixin, models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(blank=True, null=True)

    class Meta:
        db_table = "deals_cardealatrributetype"


class CarDealAttribute(DealsAppMixin, models.Model):
    deal = models.ForeignKey('CarDeal', on_delete=models.CASCADE)
    attr_type = models.ForeignKey('CarDealAttributeType', on_delete=models.CASCADE)
    value = models.CharField(max_length=250)

    class Meta:
        db_table = "deals_cardealattribute"
