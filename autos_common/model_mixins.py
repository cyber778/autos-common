from django.contrib.contenttypes.fields import GenericForeignKey
from django.db import models
from django.utils import timezone



class ScheduledPublishMixin(models.Model):
    """
    Mixin to enable scheduled publishing on a given model. This mixin will add the `future_publish_date`
    column to the child model, and the child model is responsible for implementing these functions:
    * `scheduled_publish` which performs the actual scheduled publish operation
    * `ready_for_scheduled_publish`, which determines if a publish should proceed (e.g. if the user
    hasn't yet hit "publish" to set the status to FUTURE_QA or whatever)

    Usage:
    > class MyModel(ScheduledPublishMixin, models.Model):
    >     ...
    >     def scheduled_publish(self):
    >         ...
    >         return "Published to QA successfully"
    >
    >     def ready_for_scheduled_publish(self):
    >         ...
    >         return False

    """
    future_publish_date = models.DateTimeField(null=True, blank=True)

    def scheduled_publish(self):
        """
        Publish this instance; this could be via the direct method, or any of the inject methods.

        :return a message about the publish
        :rtype str
        """
        raise NotImplementedError('Subclasses of ScheduledPublishMixin must implement scheduled_publish method')

    def ready_for_scheduled_publish(self):
        """
        Allows subclasses to decide if they're ready for scheduled publishing. This allows them to not
        publish, but without an exception (for example if someone chooses a future publish date but hasn't
        yet hit publish).

        Note: when implemented in subclasses, you should not check against the `future_publish_date` -
        this method is for checking everything outside of that.

        Example implementation:
        > def ready_for_scheduled_publish(self):
        >     return self.status in ('FUTURE_QA', 'FUTURE_PROD')

        :return whether or not we should proceed with the scheduled publish
        :rtype bool
        """
        raise NotImplementedError('Subclasses of ScheduledPublishMixin must implement ready_for_scheduled_publish method')

    def is_future_publish(self):
        return self.future_publish_date and self.future_publish_date > timezone.now()

    class Meta:
        abstract = True


class VersionPublisherMixin(models.Model):
    """
    The actual table for handling draft/prod versions

    For explenation regarding generic FK go to:
    https://docs.djangoproject.com/en/1.11/ref/contrib/contenttypes/#django.contrib.contenttypes.fields.GenericForeignKey

    To save you time I copied the key example:

    EXAMPLE MODEL:

    class TaggedItem(models.Model):
    tag = models.SlugField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    CODE EXAMPLE:

    from django.contrib.auth.models import User
    guido = User.objects.get(username='Guido')
    t = TaggedItem(content_object=guido, tag='bdfl')
    t.save()
    t.content_object

    DON'T DO:

    # This will fail
    TaggedItem.objects.filter(content_object=guido)
    # This will also fail
    TaggedItem.objects.get(content_object=guido)
    # Likewise, GenericForeignKeys does not appear in ModelForms.



    """
    draft_version_obj = GenericForeignKey('content_type', 'object_id')
    live_version_obj = GenericForeignKey('content_type', 'object_id')
    last_publish_date = models.DateTimeField()

    class Meta:
        abstract = True


class DealsAppMixin(models.Model):
    """ Mixin to make sure the table gets the correct name convention """
    class Meta:
        abstract = True
        app_label = "deals"
