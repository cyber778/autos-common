from setuptools import setup

setup(
    name='autos-common',
    version='1.0.0',
    description='Use same models across our 2 Autos projects (DMS & CDA AKA Autos)',
    author='Elad Silberring',
    author_email='esilberring@usnews.com',
    url='https://bitbucket.org/cyber778/autos-common',
    packages=['autos_common'],
    install_requires=['django>=1.11']
)
